package cz.vitskalicky.moodletest

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.RequestBody
import java.lang.Exception
import kotlin.system.exitProcess


import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.asRequestBody
import java.io.File


fun main(args: Array<String>) {
    val help = """
Moodle assignment submitter

USAGE
    java -jar moodle-assignment-submitter.jar <school url> <username> <password> <assignment id> <file>
    
DESCRIPTION
    Uploads and submits given file to Moodle.
    
ARGUMENTS
    school url - base url of the school's moodle e.g. https://sandbox.moodledemo.net
    
    username - User's login name
    
    password - User's password
     
    assignment id - id number of the assignment, which you get from url. Navigate to the assignment and copy the query parameter 'id' from URL. In this example it is 651: https://sandbox.moodledemo.net/mod/assign/view.php?id=651
    
    file - path to file you want to submit
    
EXAMPLE
    java -jar moodle-assignment-uploader.jar https://sandbox.moodledemo.net sam_the_student supersecretpassword 4 homework.docx
   
    """.trimIndent()
    if (args.size != 5) {
        println(help)
        exitProcess(255)
    }
    val username = args[1]
    val password = args[2]
    val url = args[0]
    val assgnmntCMID = args[3].toInt()
    val fileName = args[4]

    val client = OkHttpClient()
    val mapper = ObjectMapper()


    val token: String = try {
        val tokenRequest = Request.Builder()
            .url("$url/login/token.php?username=$username&password=$password&service=moodle_mobile_app")
            .get()
            .build()
        val tokenResponse = client.newCall(tokenRequest).execute()
        val tokenMap: Map<String, Any> = mapper.readValue(tokenResponse.body!!.string())
        (tokenMap["token"] as String?)!!
    } catch (e: Exception) {
        e.printStackTrace()
        System.err.println("Error obtaining token")
        exitProcess(1)
    }

    println("token: $token")

    val assgnmntId: Int = try {
        val assgnmntsRequest = Request.Builder()
            .url("$url/webservice/rest/server.php?wstoken=$token&wsfunction=mod_assign_get_assignments&moodlewsrestformat=json")
            .get()
            .build()

        val assgnmntsResponse = client.newCall(assgnmntsRequest).execute()

        val assgnmntsMap: Map<String, Any> = mapper.readValue(assgnmntsResponse.body!!.string())
        println("courses: $assgnmntsMap")
        val all: MutableList<Map<String, Any>> = ArrayList()
        (assgnmntsMap["courses"] as List<Map<String, Any>>).map {
            it["assignments"] as List<Any>
        }.forEach {
            it.forEach {
                all.add(it as Map<String, Any>)
            }
        }
        all.first { it["cmid"] == assgnmntCMID }["id"] as Int
    } catch (e: Exception) {
        e.printStackTrace()
        System.err.println("Error obtaining assignment ID")
        exitProcess(2)
    }

    println("assignment ID: $assgnmntId")

    val fileId: Int = try {
        val file = File(fileName)

        val body: RequestBody = MultipartBody.Builder()
            .setType(MultipartBody.FORM)
            .addFormDataPart("files", file.name, file.asRequestBody())
            .build()

        val request = Request.Builder()
            .url("$url/webservice/upload.php?token=$token")
            .post(body)
            .addHeader("Content-Type", "multipart/form-data")
            .build()

        val response = client.newCall(request).execute()

        val fileRes: List<Map<String, Any>> = mapper.readValue(response.body!!.string())
        fileRes.first()["itemid"] as Int
    } catch (e: Exception) {
        e.printStackTrace()
        System.err.println("Error uploading file")
        exitProcess(3)
    }

    println("file ID: $fileId")

    try {
        val request = Request.Builder()
            .url("$url/webservice/rest/server.php?wstoken=$token&wsfunction=mod_assign_save_submission&assignmentid=$assgnmntId&plugindata%5Bfiles_filemanager%5D=$fileId&moodlewsrestformat=json")
            .get()
            .build()

        val response = client.newCall(request).execute()
    } catch (e: Exception) {
        e.printStackTrace()
        System.err.println("Error submitting assignment")
        exitProcess(4)
    }

    println("Submitting successful")
}